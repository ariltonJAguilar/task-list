import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

//Read and writing in Json

void main() {
  runApp(MaterialApp(home: Home()));
}

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List _toDoList = [];
  Map _lastRemoved;
  int _lastRemovedPos;

  @override
  void initState() {
    super.initState();
    _readData().then((data) {
      setState(() {
        _toDoList = json.decode(data);
      });
    });
  }

  void _addToDo() {
    if (controller.text.isNotEmpty) {
      Map newItem = {"title": controller.text, "value": false};
      controller.clear();
      setState(() {
        _toDoList.add(newItem);
      });
      _saveData();
    }
  }

  void _updateToDo(bool c, int index) {
    setState(() {
      _toDoList[index]["value"] = c;
    });
    _saveData();
  }

  void _removeToDo(int index) {
    setState(() {
      _toDoList.removeAt(index);
    });
  }

  void _insertToDo(int _lastRemovedPos, Map _lastRemoved) {
    setState(() {
      _toDoList.insert(_lastRemovedPos, _lastRemoved);
    });
    _saveData();
  }

  Future<Null> _refresh() async {
    await Future.delayed(Duration(seconds: 1));
    setState(() {
      _toDoList.sort((a, b) {
        if (a["value"] && !b["value"])
          return 1;
        else if (!a["value"] && b["value"])
          return -1;
        else
          return 0;
      });
    });
    _saveData();
  }

  TextEditingController controller = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return mainScreen(
        _toDoList,
        controller,
        _addToDo,
        _updateToDo,
        _lastRemoved,
        _lastRemovedPos,
        _saveData,
        _removeToDo,
        _insertToDo,
        _refresh);
  }

//Recovering the file path with the saved data
  Future<File> _getFile() async {
    final directory = await getApplicationDocumentsDirectory();
    return File("${directory.path}/data.json");
  }

//Saving data in external file
  Future<File> _saveData() async {
    String data = json.encode(_toDoList);
    final file = await _getFile();
    return file.writeAsString(data);
  }

  //Recovering data
  Future<String> _readData() async {
    try {
      final file = await _getFile();
      return file.readAsString();
    } catch (e) {
      return null;
    }
  }
}

Widget mainScreen(
    List _toDoList,
    TextEditingController controller,
    Function _addToDo,
    Function _updateToDo,
    Map _lastRemoved,
    int _lastRemovedPos,
    Function _saveData,
    Function _removeToDo,
    Function _insertToDo,
    Function _refresh) {
  return Scaffold(
    appBar: AppBar(
      centerTitle: true,
      title: Text("Task List"),
    ),
    body: Column(
      children: [
        Container(
          padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
          child: Row(
            children: [
              Expanded(
                child: TextField(
                  controller: controller,
                  decoration: InputDecoration(
                      labelText: "New Task",
                      labelStyle: TextStyle(color: Colors.blueAccent)),
                ),
              ),
              RaisedButton(
                onPressed: () {
                  _addToDo();
                },
                color: Colors.blueAccent,
                child: Text(
                  "ADD",
                  style: TextStyle(color: Colors.white),
                ),
              )
            ],
          ),
        ),
        Expanded(
            child: RefreshIndicator(
          onRefresh: _refresh,
          child: ListView.builder(
              padding: EdgeInsets.only(top: 10),
              itemBuilder: (context, index) {
                return myCheckBox(
                    context,
                    _toDoList,
                    _updateToDo,
                    index,
                    _lastRemoved,
                    _lastRemovedPos,
                    _saveData,
                    _removeToDo,
                    _insertToDo);
              },
              itemCount: _toDoList.length),
        ))
      ],
    ),
  );
}

Widget myCheckBox(
    dynamic context,
    List _toDoList,
    Function _updateToDo,
    dynamic index,
    Map _lastRemoved,
    int _lastRemovedPos,
    Function _saveData,
    Function _removeToDo,
    Function _insertToDo) {
  return Dismissible(
    onDismissed: (direction) {
      _lastRemoved = Map.from(_toDoList[index]);
      _lastRemovedPos = index;
      _removeToDo(index);
      _saveData();
      final snack = SnackBar(
          duration: Duration(seconds: 3),
          action: SnackBarAction(
            label: "Desfazer",
            onPressed: () {
              _insertToDo(_lastRemovedPos, _lastRemoved);
            },
          ),
          content: Text("Tarefa \"${_lastRemoved["title"]}\" removida!"));
      Scaffold.of(context).removeCurrentSnackBar();
      Scaffold.of(context).showSnackBar(snack);
    },
    key: Key(DateTime.now().microsecondsSinceEpoch.toString()),
    background: Container(
        color: Colors.red,
        child: Align(
          alignment: Alignment(-0.9, 0),
          child: Icon(
            Icons.delete,
            color: Colors.white,
          ),
        )),
    direction: DismissDirection.startToEnd,
    child: CheckboxListTile(
      value: _toDoList[index]["value"],
      onChanged: (c) {
        _updateToDo(c, index);
      },
      title: Text(_toDoList[index]["title"]),
      secondary: CircleAvatar(
          child: Icon(
        _toDoList[index]["value"] ? Icons.check : Icons.error,
      )),
    ),
  );
}
