# Task List

A more advanced implementation of a task list in Flutter.

Json is used for data persistence. 

It is possible to delete a task and undo the action. It is possible to update the task list to leave incomplete ones at the top.


# Sample screens
![enter image description here](https://gitlab.com/ariltonJAguilar/task-list/-/raw/master/samples/sample1.jpg)
![enter image description here](https://gitlab.com/ariltonJAguilar/task-list/-/raw/master/samples/sample2.jpg)

![enter image description here](https://gitlab.com/ariltonJAguilar/task-list/-/raw/master/samples/sample3.jpg)
![enter image description here](https://gitlab.com/ariltonJAguilar/task-list/-/raw/master/samples/sample4.jpg)

![enter image description here](https://gitlab.com/ariltonJAguilar/task-list/-/raw/master/samples/sample5.jpg)

